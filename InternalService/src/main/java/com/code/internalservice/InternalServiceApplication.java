package com.code.internalservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;

import java.net.UnknownHostException;

@SpringBootApplication
@EnableEurekaClient
public class InternalServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InternalServiceApplication.class, args);
    }

}
