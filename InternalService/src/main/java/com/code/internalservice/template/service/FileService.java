package com.code.internalservice.template.service;

import com.code.internalservice.template.dto.response.FileResponse;
import com.code.internalservice.template.entity.File;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface FileService {

    public FileResponse save(MultipartFile file) throws IOException;

    public Optional<File>  getFile(String id);

    public List<File> getAllFiles();

    File approveFile(String id);
}
