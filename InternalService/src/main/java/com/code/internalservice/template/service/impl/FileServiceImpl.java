package com.code.internalservice.template.service.impl;

import com.code.internalservice.template.dto.response.FileResponse;
import com.code.internalservice.template.entity.File;
import com.code.internalservice.template.exception.FileNotFoundException;
import com.code.internalservice.template.exception.FileNotSupportedException;
import com.code.internalservice.template.mapper.TemplateMapper;
import com.code.internalservice.template.repository.FileRepository;
import com.code.internalservice.template.service.FileService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {

    private FileRepository fileRepository;

    private TemplateMapper templateMapper;

    public FileServiceImpl(FileRepository fileRepository, TemplateMapper templateMapper) {
        this.fileRepository = fileRepository;
        this.templateMapper = templateMapper;
    }

    @Override
    public FileResponse save(MultipartFile file) throws IOException {
        File fileEntity = new File();
        fileEntity.setName(StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename())));
        fileEntity.setContentType(file.getContentType());
        fileEntity.setData(file.getBytes());
        fileEntity.setSize(file.getSize());


        FileResponse fileResponse = null;

        //Checking if the uploaded file is correct file format
        String filename = fileEntity.getName();

        String fileExtension = Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1)).orElseThrow(NullPointerException::new);

        if (fileExtension.equals("html") || fileExtension.equals("ftl")) {

            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            String uploadDir = "/templates/" + UUID.randomUUID().toString();

            fileEntity.setFilePath(uploadDir);

            Path uploadPath = Paths.get(uploadDir);

            if (!Files.exists(uploadPath)) {
                Files.createDirectories(uploadPath);
            }

            try (InputStream inputStream = file.getInputStream()) {
                Path filePath = uploadPath.resolve(fileName);
                System.out.println(filePath.toFile().getAbsolutePath());
                Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

                fileResponse = new FileResponse(fileEntity);
            } catch (IOException e) {
                throw new IOException("Could not save uploaded file: " + fileName + " to destination directory");
            }

            fileRepository.save(fileEntity);

        }

//        FileResponse fileResponse = new FileResponse(fileEntity);
//        fileResponse.setFile_path();
        else
//            return "File not supported";
            throw new FileNotSupportedException();

//        System.out.println("File name: " + StringUtils.cleanPath(file.getOriginalFilename()));

//        return "File uploaded successfully";


        return fileResponse;

    }

    @Override
    public Optional<File> getFile(String id) {
        return fileRepository.findById(id);

//        return templateMapper.selectById(id);
    }

    @Override
    public List<File> getAllFiles() {
//        return fileRepository.findAll();
        return templateMapper.selectAll();
    }

    @Override
    public File approveFile(String id) {
        File file = fileRepository.findById(id).orElseThrow(() -> new FileNotFoundException());
        file.setApproved(true);
        fileRepository.save(file);

        return file;
    }


}
