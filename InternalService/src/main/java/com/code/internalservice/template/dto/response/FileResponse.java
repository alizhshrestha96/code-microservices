package com.code.internalservice.template.dto.response;

import com.code.internalservice.template.entity.File;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileResponse {

    private String id;

    private String name;

    private Long size;

    private String url;

    private String file_path;

    private String contentType;

    private boolean isApproved;

    public FileResponse(){

    }

    public FileResponse(File file){
        this.id = file.getId();
        this.name= file.getName();
        this.contentType = file.getContentType();
        this.size = file.getSize();
        this.isApproved = file.isApproved();
        this.setFile_path(file.getFilePath());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }
}
