package com.code.internalservice.template.exception;

public class FileNotFoundException extends RuntimeException{

    private String message;

    public FileNotFoundException(String message){
        this.message = message;
    }

    public FileNotFoundException(){

    }

}
