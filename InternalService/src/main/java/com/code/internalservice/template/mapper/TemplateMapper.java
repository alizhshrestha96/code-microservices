package com.code.internalservice.template.mapper;

import com.code.internalservice.template.entity.File;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TemplateMapper {

    File selectById(String id);

    List<File> selectAll();
}
