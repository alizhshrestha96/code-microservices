package com.code.internalservice.template.exception;

public class FileNotSupportedException extends RuntimeException{

    private String message;

    public FileNotSupportedException(String message){
        this.message = message;
    }

    public FileNotSupportedException(){

    }
}
