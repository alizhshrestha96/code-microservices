package com.code.internalservice.template.controller;

import com.code.internalservice.template.dto.response.FileResponse;
import com.code.internalservice.template.entity.File;
import com.code.internalservice.template.service.FileService;
import com.google.common.net.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/internal/files")
public class FileController {

    private FileService fileService;

    public FileController(FileService fileService){
        this.fileService = fileService;
    }

    @GetMapping("/hello")
    public String hello(){
        return "Hello";
    }

    @RolesAllowed("SENDER")
    @PostMapping()
    public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file) throws IOException {
//        try{
//            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

//            File savedFile = fileService.save(file);
            FileResponse savedFile = fileService.save(file);

//            String uploadDir = "/templates/" + savedFile.getId();
//
//            Path uploadPath = Paths.get(uploadDir);
//
//            if(!Files.exists(uploadPath)){
//                Files.createDirectories(uploadPath);
//            }
//
//            try(InputStream inputStream = file.getInputStream()) {
//                Path filePath = uploadPath.resolve(fileName);
//                System.out.println(filePath.toFile().getAbsolutePath());
//                Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
//            }catch (IOException e){
//                throw new IOException("Could not save uploaded file: " + fileName);
//            }

//            return ResponseEntity.status(HttpStatus.OK)
//                    .body(String.format("File " + " %s" + " saved successfully", file.getOriginalFilename()));
//        }catch (Exception e){
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//                    .body(String.format("Could not upload the file: %s!", file.getOriginalFilename()));
//        }

        return ResponseEntity.ok(savedFile);
    }


    @GetMapping
    @RolesAllowed("APPROVER")
    public List<FileResponse> listOfFiles(){
        return fileService.getAllFiles().stream().map(this::mapToFileResponse).collect(Collectors.toList());
    }

    private FileResponse mapToFileResponse(File file){
        String downloadURL = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/files/")
                .path(file.getId())
                .toUriString();
        FileResponse fileResponse = new FileResponse();
        fileResponse.setId(file.getId());
        fileResponse.setName(file.getName());
        fileResponse.setContentType(file.getContentType());
        fileResponse.setSize(file.getSize());
        fileResponse.setUrl(downloadURL);

        return fileResponse;
    }

    @GetMapping("/{id}")
    @RolesAllowed("APPROVER")
    public ResponseEntity<byte[]> getFile(@PathVariable String id){
//        File fileEntity = fileService.getFile(id);

//        return fileEntity == null ? ResponseEntity.notFound().build() : ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileEntity.getName() + "\"")
//                .contentType(MediaType.valueOf(fileEntity.getContentType()))
//                .body(fileEntity.getData());

        Optional<File> fileEntityOptional = fileService.getFile(id);

        if(!fileEntityOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }

        File fileEntity = fileEntityOptional.get();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileEntity.getName() + "\"")
                .contentType(MediaType.valueOf(fileEntity.getContentType()))
                .body(fileEntity.getData());
    }

    @PutMapping("/approve/{id}")
    public ResponseEntity<?> approveFile(@PathVariable("id") String id){
        return ResponseEntity.ok(fileService.approveFile(id));
    }


}
