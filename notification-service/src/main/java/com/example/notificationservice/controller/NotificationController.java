package com.example.notificationservice.controller;

import com.example.notificationservice.config.MessagingConfig;
import com.example.notificationservice.constants.MessagingConstants;
import com.example.notificationservice.dto.MailReceiver;
import com.example.notificationservice.service.EmailService;
import com.example.notificationservice.service.impl.EmailServiceImpl;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/notification")
public class NotificationController {

    private EmailService emailService;


//    private RabbitTemplate template;

    public NotificationController(EmailService emailService/*, RabbitTemplate template*/){
        this.emailService = emailService;
//        this.template = template;
    }

    @GetMapping("/home")
    public String home(){
        return "home";
    }

    @PostMapping("/sendEmail")
    @RabbitListener(queues = MessagingConstants.QUEUE)
    public ResponseEntity<?> sendEmail(@RequestBody MailReceiver mailReceiver){

        emailService.sendEmail(mailReceiver);
//        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, emailService.sendEmail(mailReceiver));
        return ResponseEntity.ok().body(true);
    }

}
