package com.example.notificationservice.service;

import com.example.notificationservice.dto.Email;
import com.example.notificationservice.dto.MailReceiver;

public interface EmailService {

    String sendEmail(MailReceiver mailReceiver);
}
