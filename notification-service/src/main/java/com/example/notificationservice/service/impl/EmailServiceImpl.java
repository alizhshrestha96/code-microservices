package com.example.notificationservice.service.impl;

import com.example.notificationservice.dto.Email;
import com.example.notificationservice.dto.MailReceiver;
import com.example.notificationservice.service.EmailService;
import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;
    private final FreeMarkerConfigurer freeMarkerConfigurer;

    public EmailServiceImpl(FreeMarkerConfigurer freeMarkerConfigurer, JavaMailSender javaMailSender) {
        this.freeMarkerConfigurer = freeMarkerConfigurer;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public String sendEmail(MailReceiver mailReceiver) {
        Map<String, String> model = new HashMap<>();
        model.put("name", mailReceiver.getName());
        model.put("phone", mailReceiver.getPhone());
        model.put("address", mailReceiver.getAddress());

        System.out.println(mailReceiver);


//        ObjectMapper mapper = new ObjectMapper();
//        MailReceiver mail = mapper.readValue(mailReceiver, MailReceiver.class);

        Email email = new Email();
        email.setTo("musemateproduction@gmail.com");
        email.setFrom("alizhshrestha@gmail.com");
        email.setSubject("Welcome to code services");
        email.setModel(model);

        Template template = null;
        try {
            template = freeMarkerConfigurer.getConfiguration().getTemplate("/email/welcome.ftl");
            this.sendEmail(email, template);
        } catch (IOException | TemplateException | MessagingException e) {
//            throw new RuntimeException(e);
            System.out.println(e.getMessage());
        }

        return "Email sent successfully!!";

    }

    private void sendEmail(Email email, Template template) throws TemplateException, IOException, MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, email.getModel());

        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        helper.setTo(email.getTo());
        helper.setText(html, true);
        helper.setSubject(email.getSubject());
        helper.setFrom(email.getFrom());

        javaMailSender.send(message);

    }

}
