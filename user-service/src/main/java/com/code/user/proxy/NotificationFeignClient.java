package com.code.user.proxy;

import com.code.user.dto.MailReceiver;
import com.code.user.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name="notification-service")
public interface NotificationFeignClient {

    @PostMapping(value = "/api/notification/sendEmail", produces = "application/json", consumes = "application/json")
    ResponseEntity<?> sendEmail(@RequestBody MailReceiver mailReceiver);
}
