package com.code.user.proxy;

import com.code.user.dto.MailReceiver;
import com.code.user.dto.response.FileResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name="internal-service")
public interface InternalFeignClient {

//    @PostMapping(value = "/api/notification/sendEmail", produces = "application/json", consumes = "application/json")
//    ResponseEntity<?> sendEmail(@RequestBody MailReceiver mailReceiver);

    @GetMapping(value="/api/internal/files"/*, produces = "application/json", consumes = "application/json"*/)
    List<FileResponse> getFiles();

//    @GetMapping(value="/api/internal/files/{file_id}")
//    FileResponse getFile(@PathVariable("file_id") String file_id);
}
