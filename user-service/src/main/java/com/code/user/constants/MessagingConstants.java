package com.code.user.constants;

public class MessagingConstants {

    public static final String QUEUE = "rabbitmq_queue";

    public static final String EXCHANGE = "rabbitmq_exchange";

    public static final String ROUTING_KEY = "rabbitmq_routingkey";

    public static final String HOST = "localhost";

    public static final String USERNAME = "guest";

    public static final String PASSWORD = "guest";
}
