package com.code.user.service;

import com.code.user.dto.response.FileResponse;
import com.code.user.entity.User;

import java.util.List;

public interface EmailSendingService {

    String sendEmail(User user, String email_template_id);


}
