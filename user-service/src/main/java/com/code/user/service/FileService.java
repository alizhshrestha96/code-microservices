package com.code.user.service;

import com.code.user.dto.response.FileResponse;

import java.util.List;

public interface FileService {
    List<FileResponse> getFiles();

}
