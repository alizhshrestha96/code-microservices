package com.code.user.service.impl;

import com.code.user.dto.response.FileResponse;
import com.code.user.proxy.InternalFeignClient;
import com.code.user.service.FileService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    private InternalFeignClient internalFeignClient;

    public FileServiceImpl(InternalFeignClient internalFeignClient){
        this.internalFeignClient = internalFeignClient;
    }

    @Override
    public List<FileResponse> getFiles() {
        return internalFeignClient.getFiles();
    }

}
