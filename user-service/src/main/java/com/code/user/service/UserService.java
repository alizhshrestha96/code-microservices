package com.code.user.service;

import com.code.user.dto.UserDto;
import com.code.user.entity.User;
//import com.code.user.model.User;

import java.io.FileNotFoundException;
import java.util.List;

public interface UserService {

    UserDto saveUser(User user);

    List<UserDto> selectAllUsers();

    UserDto selectUserById(int id);

    String updateUser(User user);

    String deleteUser(int id);

    String sendEmail(User user, String file_id) throws FileNotFoundException;
}
