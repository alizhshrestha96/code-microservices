package com.code.user.service.impl;

import com.code.user.dto.MailReceiver;
import com.code.user.dto.UserDto;
import com.code.user.dto.response.FileResponse;
import com.code.user.entity.User;
import com.code.user.mapper.UserMapper;
import com.code.user.proxy.InternalFeignClient;
import com.code.user.proxy.NotificationFeignClient;
import com.code.user.repository.UserRepository;
import com.code.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements UserService {

    private UserMapper userMapper;
    private UserRepository userRepository;

    private Tracer tracer;

    private NotificationFeignClient notificationFeignClient;

    private InternalFeignClient internalFeignClient;

    @Autowired
    private WebClient.Builder loadBalancedWebClientBuilder;

    public UserServiceImpl(UserMapper userMapper,
                           UserRepository userRepository,
                           Tracer tracer,
                           NotificationFeignClient notificationFeignClient,
                           InternalFeignClient internalFeignClient) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.tracer = tracer;
        this.notificationFeignClient = notificationFeignClient;
        this.internalFeignClient = internalFeignClient;
    }

    @Override
    public UserDto saveUser(User user) {
//        int user_id = userMapper.insertUser(user);
//
//        User getUser = userMapper.selectUserById(user_id);
//
//        System.out.println("count: " + user_id);
//        UserDto userDto = new UserDto(getUser);
//
//        if(user_id>0){
//            return userDto;
//        }else{
//            return null;
//        }

        userRepository.save(user);

        //email service sending methods

        //

        UserDto userDto = new UserDto(user);

        //For distributed tracing using sleuth and zipkin
        Span notificationServiceLookup = tracer.nextSpan().name("NotificationServiceLookup");

        try (Tracer.SpanInScope spanInScope = tracer.withSpan(notificationServiceLookup.start())) {
            String strHello =  loadBalancedWebClientBuilder.build().get()
                    .uri("http://notification-service/api/notification/home")
//                .uri("http://localhost:8081/hello",
//                        uriBuilder -> uriBu ilder.queryParam("skucode", skuCodes).build())
                    .retrieve()
                    .bodyToMono(String.class)
//                .bodyToMono(InventoryResponse[].class)
                    .block();
//
            System.out.println("This is from the webclient response from notification service: " + strHello);

        }finally {
            notificationServiceLookup.end();
        }

        //sending email
        MailReceiver mailReceiver = new MailReceiver();
        mailReceiver.setName(user.getFirst_name() + " " + user.getLast_name());
        mailReceiver.setPhone(user.getPhone());
        mailReceiver.setAddress(user.getAddress());
        mailReceiver.setEmail(user.getEmail());

        notificationFeignClient.sendEmail(mailReceiver);

        return userDto;
    }

    @Override
    public String sendEmail(User user, String file_id) throws FileNotFoundException {
        List<FileResponse> files = internalFeignClient.getFiles();

        FileResponse fileResponse = files
                .stream()
                .filter(f -> f.getId().equals(file_id))
                .findAny()
                .orElseThrow(() -> new FileNotFoundException());




        //sending email
        MailReceiver mailReceiver = new MailReceiver();
        mailReceiver.setName(user.getFirst_name() + " " + user.getLast_name());
        mailReceiver.setPhone(user.getPhone());
        mailReceiver.setAddress(user.getAddress());
        mailReceiver.setEmail(user.getEmail());

        notificationFeignClient.sendEmail(mailReceiver);

        return "Email Sending....";
    }

    @Override
    public List<UserDto> selectAllUsers() {

        List<User> users = userMapper.selectUsers();

        List<UserDto> userDtos = users.stream().map(u -> new UserDto(u)).collect(Collectors.toList());

        return userDtos;
    }

    @Override
    public UserDto selectUserById(int id) {

        User user = userMapper.selectUserById(id);

        UserDto userDto = new UserDto(user);

        return user != null ?  userDto : null;

//        if (user != null) {
//            return userDto;
//        } else {
//            return null;
//        }
    }

    @Override
    public String updateUser(User user) {

        int res = userMapper.updateUser(user);

        return res>0 ? "User updated Successfully": "Update failed";

//        if (res > 0) {
//            return "User updated Successfully";
//        } else {
//            return "Update failed!!";
//        }

    }

    @Override
    public String deleteUser(int id) {
        int res = userMapper.deleteUser(id);

        return res>0 ? "User deleted Successfully": "Delete failed";
//
//        if (res > 0) {
//            return "User deleted successfully";
//        } else {
//            return "Deletion failed!!";
//        }
    }


}
