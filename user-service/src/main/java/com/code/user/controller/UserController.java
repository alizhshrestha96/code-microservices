package com.code.user.controller;

//import com.code.user.model.User;

import com.code.user.constants.MessagingConstants;
import com.code.user.entity.User;
import com.code.user.service.UserService;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;


@RestController
@RequestMapping("/api/user")
public class UserController {

//    Logger logger = Logger.getLogger(UserController.class);

    private UserService userService;

    private RabbitTemplate template;


    public UserController(UserService userService, RabbitTemplate template) {
        this.userService = userService;
        this.template = template;
    }

    @PostMapping()
//    @CircuitBreaker(name="notification", fallbackMethod = "fallbackMethod")
//    @TimeLimiter(name="notification")
//    @Retry(name="notification")
    public ResponseEntity<?> saveUser(@RequestBody User user) {
        return ResponseEntity.ok(userService.saveUser(user));
    }

    @PostMapping("/sendEmail/{file_id}")
    public ResponseEntity<?> sendEmail(@RequestBody User user, @PathVariable("file_id") String file_id) throws FileNotFoundException {
        template.convertAndSend(MessagingConstants.EXCHANGE, MessagingConstants.ROUTING_KEY, userService.sendEmail(user, file_id));
        return ResponseEntity.ok("Email Sending......");
    }

    @GetMapping
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(userService.selectAllUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") int id) {
        return ResponseEntity.ok(userService.selectUserById(id));
    }

//    @PutMapping
//    public ResponseEntity<?> updateUser(@RequestBody User user){
//        return ResponseEntity.ok(userService.updateUser(user));
//    }

    @DeleteMapping("/{id}")
    public String deleteUser(@PathVariable("id") int id) {
        userService.deleteUser(id);
        return "User deleted Successfully";
    }

//    public String fallbackMethod(){
//        return "Oops! Something is wrong..";
//    }


}
