package com.code.user.controller;

import com.code.user.dto.response.FileResponse;
import com.code.user.service.FileService;
import com.code.user.service.impl.EmailSendingServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user/file")
public class FileController {

    private FileService fileService;

    public FileController(FileService fileService){
        this.fileService = fileService;
    }

    @GetMapping
    public List<FileResponse> getFiles(){
        return fileService.getFiles();
    }
}
