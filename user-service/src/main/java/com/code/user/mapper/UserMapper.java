package com.code.user.mapper;

//import com.code.user.model.User;
import com.code.user.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    List<User> selectUsers();

    int insertUser(User user);

    User selectUserById(int id);

    int updateUser(User user);

    int deleteUser(int id);


}
